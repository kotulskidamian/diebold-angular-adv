# GIT 
cd ..
git clone https://bitbucket.org/ev45ive/diebold-angular-adv.git diebold-angular-adv
cd diebold-angular-adv
npm i 
npm run start

git pull -u origin master

# Po ćwiczeniu - stash+pull
git stash -u -m "Moje zmiany"
git pull -f 

# Wersje 
node -v
npm -v
code -v 
git --version
chrome -v 

# VSCode
- Angular Language Service https://marketplace.visualstudio.com/items?itemName=Angular.ng-template 

- Angular 10 Snippets - TypeScript, Html, Angular Material, ngRx, RxJS & Flex Layout https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

- Switcher https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

- Nx Console https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console

- Paste JSON as Code https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://augury.rangle.io/

# Mac/Ubuntu - No Sudo
https://medium.com/@Mandysidana/using-npm-install-without-sudo-2de6f8a9e1a3

# Powershell
ng.cmd version 

# Update Angular CLI
npm cache clean --force 
npm uninstall -g @angular/cli 
npm i -g @angular/cli@lastest

npm update @angular/cli@latest -g

# Instalacje
npm install -g @angular/cli 
ng version 
Angular CLI: 11.0.3
ng help



# Nowy projekt
<!-- ng new diebold-angular-adv --interactive -->
<!-- ng new diebold-angular-adv --routing --strict --style scss -->
cd ..
ng new diebold-angular-adv
? Do you want to enforce stricter type checking and stricter bundle budgets in the workspace?
  This setting helps improve maintainability and catch bugs ahead of time.
  For more information, see https://angular.io/strict Yes
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss ]

cd diebold-angular-adv
ng s -o

# Playlists module (routed)
ng generate @schematics/angular:module --name=playlists --module=app --route=playlists --routing --no-interactive
ng generate @schematics/angular:component --name=playlists/components/playlist-list --no-interactive
ng generate @schematics/angular:component --name=playlists/components/playlist-list-item --no-interactive
ng generate @schematics/angular:component --name=playlists/components/playlist-details --no-interactive
ng generate @schematics/angular:component --name=playlists/components/playlist-edit-form --no-interactive

# Shared Module (exported)
ng generate @schematics/angular:module --name=shared --module=playlists --no-interactive 
ng generate @schematics/angular:pipe --name=shared/yesno --export --no-flat --no-interactive


# Core Module
ng generate @schematics/angular:module --name=core --module=app --no-interactive
ng g i core/model/playlist


# Music-Search module
ng generate @schematics/angular:module --name=music-search --module=app --route=music --routing --no-interactive
ng generate @schematics/angular:component --name=music-search/components/search-form --no-interactive
ng generate @schematics/angular:component --name=music-search/components/search-results --no-interactive
ng generate @schematics/angular:component --name=music-search/components/album-card --no-interactive
ng g i core/model/search
ng g class core/mock/albums
ng g service core/services/music-search --flat false  --no-interactive


- Inject Mocks into service
- Inject service into component
- Get mock data from service
- Input mock data (results) into results comopnent
- Input 1 result into each album card 
- Display first of images + album name

- Form emits query string to parent  when 'search' is clicked
- parent console.log query string when 'search' is clicked

# OAuth2
https://manfredsteyer.github.io/angular-oauth2-oidc/docs/index.html

npm i angular-oauth2-oidc --save

ng g m core/auth
ng g s core/auth/auth

# Rxjs
rxmarbles.com
https://rxjs-dev.firebaseapp.com/api/index/function/range
https://rxviz.com/examples/pause-and-resume
https://www.learnrxjs.io/learn-rxjs/operators

# Interceptors
 ng generate @schematics/angular:interceptor --name=core/interceptors/auth --no-interactive 

# Album detals 

http://localhost:4200/music/albums/5Tby0U5VndHW0SomYO7Id7

ng g c music-search/containers/album-details

# Whats next:
https://material.angular.io/cdk/drag-drop/overview
https://ngrx.io/
https://angular.io/tutorial 

https://www.linkedin.com/in/mateuszkulesza/
