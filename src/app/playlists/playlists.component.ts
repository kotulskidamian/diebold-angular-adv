import { Component, Inject, OnInit } from '@angular/core';
import { Playlist } from '../core/model/playlist';
import { PlaylistsService } from '../core/services/playlists/playlists.service';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {
  mode: 'details' | 'edit' = 'details'

  playlists: Playlist[] = []
  selected?: Playlist

  selectPlaylistById(id: Playlist['id']) {
    this.selected = this.selected?.id === id ? undefined : this.playlists.find(p => p.id === id)
  }

  switchToEdit() { this.mode = 'edit' }
  switchToDetails() { this.mode = 'details' }
  savePlaylist(draft: Playlist) {
    this.service.savePlaylist(draft)
    this.playlists = this.service.getPlaylists()
    this.selected = (draft)
    this.switchToDetails()
  }

  constructor(
    // @Inject(PlaylistsService)
    private service: PlaylistsService
  ) { 
    // this.playlists = this.service.getPlaylists()
    // this.service = new PlaylistsService([])
    this.service.makeCurrentUserPlaylistsRequest().subscribe(res => {
      this.playlists = res;
    })
  }

  ngOnInit(): void {
  }

}
