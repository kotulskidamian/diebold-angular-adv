import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/playlist';

// NgForOf
// NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  @Input() playlists: Playlist[] = []

  @Input() selected?: Playlist['id'] 

  @Output() selectedChange = new EventEmitter<Playlist['id']>();

  select(select: Playlist) {
    this.selectedChange.emit(select.id)
  }

  trackFn(index: number, item: Playlist) {
    return item.id
  }

  checking(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    console.log('checking list...')
  }

  constructor() { }

  ngOnInit(): void {
  }

}
