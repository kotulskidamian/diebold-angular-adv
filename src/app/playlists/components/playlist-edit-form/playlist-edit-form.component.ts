import { AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, DoCheck, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppComponent } from 'src/app/app.component';
import { Playlist } from 'src/app/core/model/playlist';

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistEditFormComponent implements OnInit, OnChanges, DoCheck, AfterViewInit, AfterViewChecked, OnDestroy {

  counter = 0
  @Output() cancel = new EventEmitter<void>();
  @Output() save = new EventEmitter<Playlist>();

  cancelClicked() { this.cancel.emit() }
  saveSubmitted(formRef: NgForm) {
    const draft = {
      ...this.playlist,
      // ...formRef.value,
      name: formRef.value.name,
      public: formRef.value.public,
      description: formRef.value.description,
    }
    this.save.emit(draft)
    debugger
  }

  @ViewChild('nameRef', { read: ElementRef })
  nameRef?: ElementRef<HTMLInputElement>

  @ViewChild('formRef'/* or NgForm */, { read: NgForm })
  formRef?: NgForm

  @Input() playlist!: Playlist
  @Input() draft!: Playlist

  constructor(private app: AppComponent) {
    console.log('constructor')
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.    
    console.log('ngOnChanges', changes)
    // this.draft = { ...this.playlist } /* , tracks: [...this.playlist.tracks] } */
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    if (!this.playlist) { throw new Error('playlist is required') }
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    console.log('ngDoCheck')
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log('ngAfterViewInit')
    // console.log('ref', this.nameRef)
    this.nameRef?.nativeElement.focus()
    
    // ExpressionChangedAfterItHasBeenCheckedError
    
    setTimeout(() => {
      this.app.title = this.playlist.name;
      this.formRef?.setValue({
        name: this.playlist.name,
        public: this.playlist.public,
        description: this.playlist.description,
      })
    })
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    console.log('ngAfterViewChecked')
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('ngOnDestroy')
  }

}
