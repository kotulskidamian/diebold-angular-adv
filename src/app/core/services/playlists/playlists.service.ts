import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Playlist } from '../../model/playlist';
import { PlaylistsResponse } from '../../model/search';
import { API_URL, PlaylistsSource, PLAYLISTS_INIT_DATA } from '../../tokens';


@Injectable({
  // providedIn: CoreModule
  providedIn: 'root'
})
export class PlaylistsService implements PlaylistsSource{

  private playlists = new Observable<Playlist[]>();

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private api_url: string,
    @Optional() @Inject(PLAYLISTS_INIT_DATA) private initialPlaylists: Playlist[] = []) {

    console.log(initialPlaylists);
  }

  getPlaylists() {
    // const temp = this.makeCurrentUserPlaylistsRequest().pipe(
    //   tap(p => console.log(p))
    // ).subscribe();

    return this.initialPlaylists;
  }

  savePlaylist(draft: Playlist) {
    this.initialPlaylists = this.initialPlaylists.map(
      p => p.id === draft.id ? draft : p)
  }

  makeCurrentUserPlaylistsRequest(){
    const endpoint = `${this.api_url}/me/playlists`;
    const result = this.http.get<PlaylistsResponse>(endpoint).pipe(
      map(res => res.items)
    );
    
    return result;
  }

}
