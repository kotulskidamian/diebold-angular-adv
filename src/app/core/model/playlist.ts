
import { Image } from './search'
 
export interface Playlist {
  id: string;
  name: string;
  public: boolean;
  description: string;
  images: Image[];
}
