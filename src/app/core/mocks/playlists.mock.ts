import { Playlist } from '../model/playlist';

export const playlistsMock: Playlist[] = [
  {
    id: '123',
    name: 'Awesome playlist 123',
    public: true,
    description: 'I love this playlist',
    images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
  {
    id: '234',
    name: 'Awesome playlist 234',
    public: false,
    description: 'I hate this playlist',
    images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
  {
    id: '345',
    name: 'Awesome playlist 345',
    public: true,
    description: 'I know this playlist',
    images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
]
