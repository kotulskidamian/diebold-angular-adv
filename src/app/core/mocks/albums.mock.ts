import { Album } from '../model/search';


export const albumsMock: Readonly<Pick<Album, 'id' | 'name' | 'images'>>[] = [
  {
    id: '123',
    name: 'Album 123',
    images: [{ url: 'https://www.placecage.com/c/300/300', width: 300, height: 300 }]
  },
  {
    id: '345',
    name: 'Album 345',
    images: [{ url: 'https://www.placecage.com/c/400/400', width: 300, height: 300 }]
  },
  {
    id: '456',
    name: 'Album 456',
    images: [{ url: 'https://www.placecage.com/c/200/300', width: 300, height: 300 }]
  },
]

// type PartialAlbum = {
//   id: Album['id']
//   name: Album['name']
//   images: Album['images']
// }

// type AlbumKeys = 'id' | 'name' | 'images'

// type PartialAlbum = {
//   [k in AlbumKeys]: Album[k]
// }

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

// type Pick<T, K extends keyof T> = {
//   [k in K]: T[k]
// }

// type Awesome<T,K extends keyof T> = Partial<Readonly<Pick<T, K>>>

type Immutable<T> = {
  readonly [K in keyof T]: Immutable<T[K]>;
}