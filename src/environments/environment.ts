// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'angular-oauth2-oidc/auth.config';

export const environment = {
  production: false,
  api_url: 'https://api.spotify.com/v1',
  authLoginFlowConfig: <AuthConfig>{
    // OpenID:
    oidc: false,
    // // Url of the Identity Provider
    // issuer: 'https://idsvr4.azurewebsites.net',

    customQueryParams: {
      // Force user to confirm every time
      // show_dialog: 'true'
    },

    // Debug
    clearHashAfterLogin: false,
    requireHttps: false,
    // silentRefreshShowIFrame: true,
    // timeoutFactor: 0.1,

    // OAuth:
    loginUrl: 'https://accounts.spotify.com/authorize',

    // URL of the SPA to redirect the user to after login
    redirectUri: window.location.origin + '/index.html',


    // URL of the SPA to redirect the user after silent refresh
    silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',

    // defaults to true for implicit flow and false for code flow
    // as for code code the default is using a refresh_token
    // Also see docs section 'Token Refresh'
    // useSilentRefresh: true,


    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: 'fa07e62944e648c59f5380b0de60eb67',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',
    // responseType: 'code',
    responseType: 'token',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    // scope: 'openid profile email offline_access api',
    scope: '',

    showDebugInformation: true,
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
